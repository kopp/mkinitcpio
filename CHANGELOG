mkinitcptio - Arch Linux initramfs generation tools 

CHANGES WITH v34:

        Announcement of future breaking changes:

        * Preset variables *_efi_image are deprecated in favor of *_uki and
          option --uefi is deprecated in favor with --uki. The old variables and
          options still work, but emit a deprecation warning.

        Changes in mkinitcpio:

        * Preserve relativity of symbolic links that reside in the same
          directory as their targets.

        * Fix symbolic link creation and create target files with correct
          permissions.

        * Document add_udev_rule that has been available since v31.

        * Instead of using stat to get the file permissions and install to copy
          it, use cp unless custom permissions are specified. This improves
          overall speed of file additions.

        * Existing test suite is migrated to bats (Bash Automated Testing
          System) and more tests are now available.

        * Ensure /proc/cmdline is read as text and make sure to append a newline
          and NUL to the .cmdline embedded in unified kernel images. This works
          around an issue where /proc/cmdline contains garbage.

        * The kernel-install plugin now supports generating unified kernel
          images.

        * Warn if the interpreter (from the shebang) is missing when adding
          "binaries" that are actually scripts.

        Changes in hooks:

        * hooks/shutdown once again works when /run/initramfs/ already exists in
          the initramfs.

        * install/autodetect looks up modules in the target kernel version
          instead of the currently running one.

        * install/consolefont, install/keymap and install/sd-vconsole now
          declare vconsole.conf supported variables as local to prevent
          conflicts with misconstructed hooks may set these variables globally.

        * install/memdisk uses the add_udev_rule function available since v31.

        Contributions from: Adam Maroti, Christian Hesse, Geert Hendrickx,
        Hector Martin, Morten Linderud, Tcc, Tobias Powalowski, nl6720, rogueai

        – 2022-12-07

CHANGES WITH v33:

        Announcements:
    
        * Development has moved to the Arch Linux GitLab instance. The new
          URL is https://gitlab.archlinux.org/archlinux/mkinitcpio/mkinitcpio
    
        Changes in mkinitcpio:
        
        * Introduce DECOMPRESS_MODULES in mkinitcpio.conf. Allows the user to
          toggle if modules should be recompressed during initramfs creation or
          not. Defaults to "yes".

        * Support UKI generation on AArch64.

        * Introduce a new --remove switch which allows mkinitcpio to remove all
          mkinitcpio generated files. Only works together
          with the -p/-P switches currently.

        * In the initramfs the file /etc/os-release, symlinked to
          /etc/initrd-release, is now included. systemd enabled initramfs
          environments might need this file.

        * Supports finding kernel version in gzipped non-x86 kernels.

        * Try to properly resolve symlinks when adding files into the initramfs.

        Changes in hooks:

        * install/fsck now includes the e2fsck binary and symlinks fsck.ext*
          utilities to this binary.

        * install/fsck will no longer attempt to include nonexistent fsck
          helpers.

        * install/kms will attempt to include modules that implement the privacy
          screen feature. Currently the module names are hardcoded.

        Changes in packaging:

        * mkinitcpio-remove and mkinitcpio-install are now unified into a single
          script installed to 'libalpm/scripts/mkinitcpio'.

        Contributions from: 0x7F, Felix Yan, Hugo Osvaldo Barrera,
        Morten Linderud, nl6720, rogueai, Simon Brüggen, Tobias Powalowski

        – 2022-11-20
